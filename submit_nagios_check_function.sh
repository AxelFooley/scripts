#################################################################################
#  Bash function to include when creating a nagios plugin			#
#  that make use of passive checks						#
#										#
#  Simply configure nagios host (how you named the client into nagios conf)	#
#  service name, nagios cmd.cgi url, user and password and you're ready to go!	#
#  										#
#  Usage: Include this script into your plugin and then call the function	#
#  	submit_nagios_check passing the exit state as first parameter		#
#	and the message as the second parameter					#
#################################################################################  

submit_nagios_check () {

case $1 in
	ok)
		STATE=0
		MSG="$2"
	;;
	warning)
		STATE=1
		MSG="$2"
	;;
	critical)
		STATE=2
		MSG="$2"
	;;
	*)
		STATE=3
		MSG="$2"
	;;
esac

# Curl Parameters
NAGIOS_HOST=CLIENT HOSTNAME INTO NAGIOS CONF
NAGIOS_SERVICE="CONFIGURED+SERVICE+NAME"
NAGIOS_URL="CMD.CGI URL"
USER="NAGIOS AUTH USER"
PASS="NAGIOS AUTH PASSWORD"
###


 curl -k --silent --show-error \
     --data "cmd_typ=30" \
     --data "cmd_mod=2" \
     --data "host=$NAGIOS_HOST" \
     --data "service=$NAGIOS_SERVICE" \
     --data "plugin_state=$STATE" \
     --data "plugin_output=$MSG" \
     --data "btnSubmit=Commit" \
     $NAGIOS_URL -u $USER:$PASS -o /dev/null
}
