#!/bin/bash

# Exit codes
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

#Dependancy check
dpkg-query -W -f='${Status} ${Version}\n' curl &> /dev/null
if [ $? = 1 ]
then
        echo "UNKNOWN It seems that \"curl\" isn't installed into your system"
        echo "Run \"apt-get install curl\" and try again"
        exit $STATE_UNKNOWN
fi

#Variables
EXPR=""
CURL=`which curl`
TMPFILE="/tmp/check_http_headers.out"

usage () {
cat << EOF
        Check HTTP Headers with expression parsing.
        This plugin connects to a given host to download and parse the http response headers, and parse them for searching
        the given expression.
        It exit with CRITICAL status if expression is found, and with OK status if not.

        Usage: $0 -H [-u|-e|-p|-t]

        Options:
                -H: Hostname or IP address. Required option.
                -u: URL to GET.
                -e: Expression to match into response headers.
		-p: Port
		-t: Timeout in seconds. Default: 10.
EOF
}

# Option parsing
while getopts ":H:u:e:t:p:" opt; do
	case $opt in
		H)
			HOST=$OPTARG
			;;
		u)
			URL=$OPTARG
			;;
		e)
			EXPR=$OPTARG
			;;
		p)
			PORT=$OPTARG
			;;
		t)
			TIMEOUT=$OPTARG
			;;
		\?)
			echo "Invalid option: -$OPTARG"
			usage
			exit $STATE_UNKNOWN
			;;
		:)
			echo "Option -$OPTARG requires an argument."
			usage
			exit $STATE_UNKNOWN
			;;
	esac
done

#Pre flight check
if [ $# -lt "1" ]
then
    usage
    exit $STATE_UNKNOWN
elif [[ -z $HOST || -z $EXPR ]]
then
	echo "Error: you must provide an hostname/IP to connect to and an expression to search for."
	exit $STATE_UNKNOWN
fi

#Take off

if [ -z "$TIMEOUT" ]
then
	TIMEOUT=10
fi

if [ ! -z "$PORT" ]
then
	$CURL --connect-timeout $TIMEOUT -s -f -I http://$HOST:$PORT/$URL -o $TMPFILE
else
	$CURL --connect-timeout $TIMEOUT -s -f -I http://$HOST/$URL -o $TMPFILE
fi

#HTTP response codes management
case $? in
	6)
		echo "Error: Could not resolve host."
		exit $STATE_UNKNOWN
		;;
	7)
		echo "Error: Could not connect to host."
		exit $STATE_UNKNOWN
		;;
	9)
		echo "Error: Access denied."
		exit $STATE_UNKNOWN
		;;
	22)
		echo "Error: File not found"
		exit $STATE_UNKNOWN
		;;
	28)
		echo "Error: Operation timed out."
		exit $STATE_UNKNOWN
		;;
	47)
		echo "Error: Too many redirects."
		exit $STATE_UNKNOWN
		;;
	\?)
		RESPONSE=`curl -s -f -I http://$HOST/$URL|head -n1`
		echo "Error: Operation failed"
		echo "Response code was: $RESPONSE"
		exit $STATE_UNKNOWN
		;;
esac

grep $EXPR $TMPFILE

if [ "$?" == "0" ]
then
	echo "CRITICAL - Expression Found: `grep $EXPR $TMPFILE`"
	exit $STATE_CRITICAL
else
	echo "OK - Expression not found"
	exit $STATE_OK
fi
