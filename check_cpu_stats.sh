#!/bin/bash

#########################################################################################################################################
#	This Nagios plugin make use of "vmstat" tool to collect statistics about cpu usage (user load, system load, cpu wait)		#
#	and print them both human readable than in perfdata syntax.									#
#	You can set warning and critical thresholds for all monitored parameters or 							#
#	choose to not set any, and accept default values (see below)									#
#########################################################################################################################################

VMSTAT=`which vmstat`

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

# Plugin parameters value if not defined
WARNING_THRESHOLD=${WARNING_THRESHOLD:="70"}
CRITICAL_THRESHOLD=${CRITICAL_THRESHOLD:="90"}
INTERVAL_SEC=${INTERVAL_SEC:="1"}
NUM_REPORT=${NUM_REPORT:="3"}
U_CPU_W=${WARNING_THRESHOLD}
S_CPU_W=${WARNING_THRESHOLD}
IO_CPU_W=${WARNING_THRESHOLD}
U_CPU_C=${CRITICAL_THRESHOLD}
S_CPU_C=${CRITICAL_THRESHOLD}
IO_CPU_C=${CRITICAL_THRESHOLD}

# Plugin variable description
PROGNAME=$(basename $0)

if [ ! -x $VMSTAT ]; then
	echo "UNKNOWN: vmstat not found or is not executable by the nagios user."
	exit $STATE_UNKNOWN
fi

print_usage() {
	echo ""
	echo "$PROGNAME - CPU Utilization check script for Nagios"
	echo ""
	echo "Usage: $0 [flags]"
	echo ""
	echo "Flags:"
	echo "  -w  <number> : Global Warning level in % for user/system/io-wait cpu (default : 70)"
	echo "  -uw <number> : Warning level in % for user cpu"
	echo "  -iw <number> : Warning level in % for IO_wait cpu"
	echo "  -sw <number> : Warning level in % for system cpu"
	echo "  -c  <number> : Global Critical level in % for user/system/io-wait cpu (default : 90)"
	echo "  -uc <number> : Critical level in % for user cpu"
	echo "  -ic <number> : Critical level in % for IO_wait cpu"
	echo "  -sc <number> : Critical level in % for system cpu"
	echo "  -i  <number> : Interval in seconds for vmstat (default : 1)"
	echo "  -n  <number> : Number of report for vmstat (default : 3)"
	echo "  -h  Show this page"
	echo ""
    echo ""
}

while [ $# -gt 0 ]; do
    case "$1" in
        -h | --help)
            print_usage
            exit $STATE_OK
            ;;
        -w | --warning)
            shift
            WARNING_THRESHOLD=$1
			U_CPU_W=$1
			S_CPU_W=$1
			IO_CPU_W=$1
        ;;
        -c | --critical)
			shift
            CRITICAL_THRESHOLD=$1
			U_CPU_C=$1
			S_CPU_C=$1
			IO_CPU_C=$1
		;;
        -uw | --uwarn)
        	shift
			U_CPU_W=$1
        ;;
        -uc | --ucrit)
        	shift
			U_CPU_C=$1
        ;;
        -sw | --swarn)
        	shift
			S_CPU_W=$1
		;;
        -sc | --scrit)
        	shift
			S_CPU_C=$1
        ;;
        -iw | --iowarn)
        	shift
			IO_CPU_W=$1
		;;
        -ic | --iocrit)
        	shift
			IO_CPU_C=$1
		;;
        -i | --interval)
        	shift
			INTERVAL_SEC=$1
		;;
        -n | --number)
        	shift
        	NUM_REPORT=$1
        ;;        
        *)  echo "Unknown argument: $1"
            print_usage
            exit $STATE_UNKNOWN
            ;;
        esac
shift
done

CPU_REPORT=`vmstat -n $INTERVAL_SEC $NUM_REPORT | tail -1`
CPU_USER=`echo $CPU_REPORT | awk '{ print $13 }'`
CPU_SYSTEM=`echo $CPU_REPORT | awk '{ print $14 }'`
CPU_IOWAIT=`echo $CPU_REPORT | awk '{ print $16 }'`
CPU_IDLE=`echo $CPU_REPORT | awk '{ print $15 }'`

if [ ${CPU_IOWAIT} -ge ${IO_CPU_C} -o ${CPU_USER} -ge ${U_CPU_C} -o ${CPU_SYSTEM} -ge ${S_CPU_C}  ];
then
	echo "CPU CRITICAL : user=${CPU_USER}% system=${CPU_SYSTEM}% iowait=${CPU_IOWAIT}% idle=${CPU_IDLE}% | cpu_user=${CPU_USER}%;${U_CPU_W};${U_CPU_C}; cpu_sys=${CPU_SYSTEM}%;${S_CPU_W};${S_CPU_C}; cpu_iowait=${CPU_IOWAIT}%;${IO_CPU_W};${IO_CPU_C}; cpu_idle=${CPU_IDLE}%;"
	exit $STATE_CRITICAL
fi

# Are we in a warning state?
if [ ${CPU_IOWAIT} -ge ${IO_CPU_W} -o ${CPU_USER} -ge ${U_CPU_W} -o ${CPU_SYSTEM} -ge ${S_CPU_W}  ];
then
	echo "CPU WARNING : user=${CPU_USER}% system=${CPU_SYSTEM}% iowait=${CPU_IOWAIT}% idle=${CPU_IDLE}% | cpu_user=${CPU_USER}%;${U_CPU_W};${U_CPU_C}; cpu_sys=${CPU_SYSTEM}%;${S_CPU_W};${S_CPU_C}; cpu_iowait=${CPU_IOWAIT}%;${IO_CPU_W};${IO_CPU_C}; cpu_idle=${CPU_IDLE}%;"
	exit $STATE_WARNING
fi

# If we got this far, everything seems to be OK - IDLE has no threshold
echo "CPU OK : user=${CPU_USER}% system=${CPU_SYSTEM}% iowait=${CPU_IOWAIT}% idle=${CPU_IDLE}% | cpu_user=${CPU_USER}%;${U_CPU_W};${U_CPU_C}; cpu_sys=${CPU_SYSTEM}%;${S_CPU_W};${S_CPU_C}; cpu_iowait=${CPU_IOWAIT}%;${IO_CPU_W};${IO_CPU_C}; cpu_idle=${CPU_IDLE}%;"
exit $STATE_OK
